<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-currency library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PHPUnit\Framework\TestCase;
use Yii2Module\Yii2Currency\CurrencyModule;

/**
 * CurrencyModuleTest test file.
 * 
 * @author Anastaszor
 * @covers \Yii2Module\Yii2Currency\CurrencyModule
 *
 * @internal
 *
 * @small
 */
class CurrencyModuleTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var CurrencyModule
	 */
	protected CurrencyModule $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new CurrencyModule('currency');
	}
	
}
