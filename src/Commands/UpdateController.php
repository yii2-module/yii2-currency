<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-currency library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2Currency\Commands;

use DateInterval;
use DateTimeImmutable;
use RuntimeException;
use Throwable;
use yii\console\ExitCode;
use yii\db\Query;
use Yii2Module\Helper\Commands\ExtendedController;
use Yii2Module\Yii2Currency\Components\CurrencyRateUpdater;
use Yii2Module\Yii2Currency\Components\DuckduckgoWebsite;
use Yii2Module\Yii2Currency\Models\CurrencyCurrency;
use Yii2Module\Yii2Currency\Models\CurrencyRate;
use Yii2Module\Yii2Currency\Models\CurrencyRateHistory;

/**
 * UpdateController class file.
 * 
 * This class represents the update command to get rate data from ddg api.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.CouplingBetweenObjects")
 */
class UpdateController extends ExtendedController
{
	
	/**
	 * Do all the actions.
	 * 
	 * @return integer
	 */
	public function actionAll() : int
	{
		$this->actionRates();
		$this->actionCompact();
		
		return ExitCode::OK;
	}
	
	/**
	 * Rates model action. This action gathers all the parent caegories
	 * from the module's website and stores them into the database.
	 * 
	 * @return integer the error code, 0 if no error
	 */
	public function actionRates() : int
	{
		return $this->runCallable(function() : int
		{
			$website = new DuckduckgoWebsite($this->getLogger());
			$rateUpdater = new CurrencyRateUpdater();
			
			/** @var array<integer, CurrencyCurrency> $currencies */
			$currencies = CurrencyCurrency::find()->all();
			
			foreach($currencies as $fromCurrency)
			{
				\sleep(1);
				
				try
				{
					$rateUpdater->saveRates($website->pullRates($fromCurrency, $currencies));
				}
				catch(Throwable $e)
				{
					$message = 'Failed to pull rates {cfrom} to {cto} at {date} : {msg}';
					$context = [
						'cfrom' => $fromCurrency->code_iso_4217,
						'cto' => \implode(',', \array_map(function(CurrencyCurrency $currency) : string
						{
							return $currency->code_iso_4217;
						}, $currencies)),
						'date' => (new DateTimeImmutable())->format('Y-m-d H:i:s'),
						'msg' => $e->getMessage(),
					];
					
					$this->getLogger()->error($message, $context);
				}
			}

			return ExitCode::OK;
		});
	}
	
	/**
	 * Compact model action. This action removes records from the currency rate
	 * table and stores them into the currency rate history table.
	 *
	 * @return integer the error code, 0 if no error
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 * @SuppressWarnings("PHPMD.ExcessiveMethodLength")
	 */
	public function actionCompact() : int
	{
		return $this->runCallable(function() : int
		{
			$logger = $this->getLogger();
			$query = new Query();
			$mindatestr = (string) $query->select('min(checked)')->from('currency_rate')->limit(1)->scalar();
			if(empty($mindatestr))
			{
				/** @psalm-suppress PossiblyFalseArgument */
				$mindatestr = (new DateTimeImmutable())->add(DateInterval::createFromDateString('-1 day'))->format('Y-m-d');
			}
			
			$mindate = DateTimeImmutable::createFromFormat('Y-m-d', $mindatestr);
			if(false === $mindate)
			{
				throw new RuntimeException(\strtr('Failed to parse min date with value {val}', ['{val}' => $mindatestr]));
			}
			$logger->debug('Begin Compacting at {date}', ['date' => $mindate->format('Y-m-d')]);
			
			$minMonth = ((int) $mindate->format('Y')) * 12 + ((int) $mindate->format('m')) - 1;
			$query = new Query();
			$maxdatestr = (string) $query->select('max(checked)')->from('currency_rate')->limit(1)->scalar();
			if(empty($maxdatestr))
			{
				$maxdatestr = (new DateTimeImmutable())->format('Y-m-d');
			}
			
			$maxdate = DateTimeImmutable::createFromFormat('Y-m-d', $maxdatestr);
			if(false === $maxdate)
			{
				throw new RuntimeException(\strtr('Failed to parse max date with value {val}', ['{val}' => $maxdatestr]));
			}
			$logger->debug('Endin Compacting at {date}', ['date' => $maxdate->format('Y-m-d')]);
			
			$maxMonth = ((int) $maxdate->format('Y')) * 12 + ((int) $maxdate->format('m')) - 1;
			
			// if there is more than two month of difference, compact them
			$currencies = CurrencyCurrency::find()->all();
			
			for($monthid = $minMonth; $maxMonth - 1 > $monthid; $monthid++)
			{
				$month = ($monthid % 12) + 1;
				$monthstr = \str_pad((string) $month, 2, '0', \STR_PAD_LEFT);
				$year = (int) ($monthid / 12);
				$yearstr = \str_pad((string) $year, 4, '0', \STR_PAD_LEFT);
				$logger->info('Processing Month {year}-{month}', ['year' => $yearstr, 'month' => $monthstr]);
				
				foreach($currencies as $currencyFrom)
				{
					/** @var CurrencyCurrency $currencyFrom */
					$logger->info(
						'Processing Rates {year}-{month} From {cfrom}',
						['year' => $yearstr, 'month' => $monthstr, 'cfrom' => $currencyFrom->code_iso_4217],
					);
					
					foreach($currencies as $currencyTo)
					{
						/** @var CurrencyCurrency $currencyTo */
						if($currencyFrom->code_iso_4217 === $currencyTo->code_iso_4217)
						{
							continue;
						}
						
						$history = CurrencyRateHistory::findOne([
							'currency_from' => $currencyFrom->code_iso_4217,
							'currency_to' => $currencyTo->code_iso_4217,
							'month' => $monthid,
						]);
						if(null === $history)
						{
							$history = new CurrencyRateHistory();
							$history->currency_from = $currencyFrom->code_iso_4217;
							$history->currency_to = $currencyTo->code_iso_4217;
							$history->month = $monthid;
						}
						
						for($day = 1; 32 > $day; $day++)
						{
							$daystr = \str_pad((string) $day, 2, '0', \STR_PAD_LEFT);
							$dateDay = DateTimeImmutable::createFromFormat('Y-m-d', $yearstr.'-'.$monthstr.'-'.$daystr);
							if(false === $dateDay)
							{
								throw new RuntimeException(\strtr('Failed to parse DateTime with value {val}', ['{val}' => $yearstr.'-'.$monthstr.'-'.$daystr]));
							}
							if(((int) $dateDay->format('m')) !== $month)
							{
								continue; // 31 feb => 02 mar, avoid those cases
							}
							
							/** @var ?CurrencyRate $rate */
							$rate = CurrencyRate::findOne([
								'currency_from' => $currencyFrom->code_iso_4217,
								'currency_to' => $currencyTo->code_iso_4217,
								'checked' => $dateDay->format('Y-m-d'),
							]);
							if(null !== $rate && !empty($rate->value))
							{
								$dayprop = 'd'.$daystr;
								$history->setAttribute($dayprop, $rate->value);
							}
						}
						$logger->debug('Saving History {cfrom}/{cto}/{year}-{month}', [
							'cfrom' => $currencyFrom->code_iso_4217,
							'cto' => $currencyTo->code_iso_4217,
							'year' => $yearstr,
							'month' => $monthstr,
						]);
						if(!$history->save())
						{
							$errors = [];
							
							foreach((array) $history->getErrorSummary(true) as $error)
							{
								$errors[] = (string) $error;
							}
							
							$message = 'Failed to save {class} : {errmsg}';
							$context = ['{class}' => \get_class($history), '{errmsg}' => \implode(', ', $errors)];
							
							throw new RuntimeException(\strtr($message, $context));
						}
					}
				}
				
				$logger->info('Deleting Rates {year}-{month}', ['year' => $yearstr, 'month' => $monthstr]);
				CurrencyRate::deleteAll(['year(checked)' => $year, 'month(checked)' => $month]);
			}
			
			return ExitCode::OK;
		});
	}
	
}
