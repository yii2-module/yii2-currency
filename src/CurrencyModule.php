<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-currency library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2Currency;

use DateTimeInterface;
use InvalidArgumentException;
use PhpExtended\Money\CurrencyInterface;
use PhpExtended\Money\CurrencyProviderInterface;
use PhpExtended\Money\RateInterface;
use PhpExtended\Money\RateProviderInterface;
use yii\base\Module;
use yii\BaseYii;
use Yii2Extended\Metadata\Bundle;
use Yii2Extended\Metadata\ModuleInterface;
use Yii2Extended\Metadata\Record;
use Yii2Module\Helper\BootstrappedModule;
use Yii2Module\Yii2Currency\Components\CurrencyAdapter;
use Yii2Module\Yii2Currency\Components\RateAdapter;
use Yii2Module\Yii2Currency\Models\CurrencyCurrency;
use Yii2Module\Yii2Currency\Models\CurrencyRate;
use Yii2Module\Yii2Currency\Models\CurrencyRateHistory;

/**
 * CurrencyModule class file.
 * 
 * This pluggable module is to extract all currencies from a source and
 * stores them into a database.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.CouplingBetweenObjects")
 */
class CurrencyModule extends BootstrappedModule
{
	
	// ---- ---- ---- ---- CurrencyProviderInterface methods ---- ---- ---- ---- \\
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Money\CurrencyProviderInterface::getCurrencyFromCodeIso4217()
	 * @throws InvalidArgumentException if the code iso does not exists
	 */
	public function getCurrencyFromCodeIso4217(string $codeIso4217) : CurrencyInterface
	{
		$currency = CurrencyCurrency::findOne($codeIso4217);
		if(null === $currency)
		{
			$message = 'Impossible to find currency with code iso 4217 {value}';
			$context = ['{value}' => $codeIso4217];
			
			throw new InvalidArgumentException(\strtr($message, $context));
		}
		
		return new CurrencyAdapter($currency);
	}
	
	// ---- ---- ---- ---- RateProviderInterface methods ---- ---- ---- ---- \\
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Money\RateProviderInterface::getRateFromSourceToTargetAt()
	 */
	public function getRateFromSourceToTargetAt(CurrencyInterface $source, CurrencyInterface $target, DateTimeInterface $when) : ?RateInterface
	{
		if($source->getCodeIso4217() === $target->getCodeIso4217())
		{
			$rate = new CurrencyRate();
			$rate->currency_from = $source->getCodeIso4217();
			$rate->currency_to = $target->getCodeIso4217();
			$rate->checked = $when->format('Y-m-d');
			$rate->value = 1000000;
			
			return new RateAdapter($rate);
		}
		$rate = CurrencyRate::findOne([
			'currency_from' => $source->getCodeIso4217(),
			'currency_to' => $source->getCodeIso4217(),
			'checked' => $when->format('Y-m-d'),
		]);
		if(null === $rate)
		{
			// encode the month on the [0-11] segment
			$month = ((int) $when->format('Y')) * 12 + ((int) $when->format('m')) - 1;
			$history = CurrencyRateHistory::findOne([
				'currency_from' => $source->getCodeIso4217(),
				'currency_to' => $source->getCodeIso4217(),
				'month' => $month,
			]);
			if(null !== $history)
			{
				$day = 'd'.\str_pad($when->format('d'), 2, '0', \STR_PAD_LEFT);
				$dval = $history->getAttribute($day);
				if(null !== $dval)
				{
					$hval = ((float) $dval) / 1000000.0;
					$rate = new CurrencyRate();
					$rate->currency_from = $source->getCodeIso4217();
					$rate->currency_to = $target->getCodeIso4217();
					$rate->checked = $when->format('Y-m-d');
					$rate->value = (int) ($hval * 100000.0);
				}
			}
		}
		
		if(null === $rate)
		{
			return null;
		}
		
		return new RateAdapter($rate);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Money\RateProviderInterface::convert()
	 */
	public function convert(float $amount, CurrencyInterface $source, CurrencyInterface $target, DateTimeInterface $when) : ?float
	{
		$rate = $this->getRateFromSourceToTargetAt($source, $target, $when);
		
		if(null === $rate)
		{
			return null;
		}
		
		return $amount * $rate->getValue();
	}
	
	// ---- ---- ---- ---- ModuleInterface methods ---- ---- ---- ---- \\
	
	/**
	 * {@inheritDoc}
	 * @see \Yii2Extended\Metadata\ModuleInterface::getBootstrapIconName()
	 */
	public function getBootstrapIconName() : string
	{
		return 'currency-exchange';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Yii2Extended\Metadata\ModuleInterface::getLabel()
	 */
	public function getLabel() : string
	{
		return BaseYii::t('CurrencyModule.Module', 'Currency');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Yii2Extended\Metadata\ModuleInterface::getEnabledBundles()
	 */
	public function getEnabledBundles() : array
	{
		return [
			'currency' => new Bundle(BaseYii::t('CurrencyModule.Module', 'Currency'), [
				'currency' => (new Record(CurrencyCurrency::class, 'currency', BaseYii::t('CurrencyModule.Module', 'Currency')))->enableFullAccess(),
				'rate' => (new Record(CurrencyRate::class, 'rate', BaseYii::t('CurrencyModule.Module', 'Rate')))->enableFullAccess(),
				'rate-history' => (new Record(CurrencyRateHistory::class, 'rate-history', BaseYii::t('CurrencyModule.Module', 'Rate History')))->enableFullAccess(),
			]),
		];
	}
	
}
