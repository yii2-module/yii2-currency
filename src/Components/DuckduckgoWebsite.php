<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-currency library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2Currency\Components;

use InvalidArgumentException;
use PhpExtended\ApiComDuckduckgoSpice\ApiComDuckduckgoSpiceEndpoint;
use PhpExtended\ApiComDuckduckgoSpice\ApiComDuckduckgoSpiceResponseInterface;
use PhpExtended\DataProvider\UnprovidableJsonException;
use PhpExtended\DataProvider\UnprovidableThrowable;
use PhpExtended\HttpClient\ClientFactory;
use PhpExtended\Parser\ParseThrowable;
use PhpExtended\Reifier\ReificationThrowable;
use PhpExtended\SimpleCache\SimpleCacheLogger;
use Psr\Http\Client\ClientExceptionInterface;
use Psr\Log\LoggerInterface;
use RuntimeException;
use yii\BaseYii;
use Yii2Extended\Yii2SimpleCache\Psr16ToYii2SimpleCache;
use Yii2Module\Yii2Currency\Models\CurrencyCurrency;

/**
 * DuckduckgoWebsite class file.
 * 
 * This class represents the duckduckgo website.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.CouplingBetweenObjects")
 */
class DuckduckgoWebsite
{
	
	/**
	 * The logger.
	 * 
	 * @var LoggerInterface
	 */
	protected LoggerInterface $_logger;
	
	/**
	 * The api.
	 * 
	 * @var ApiComDuckduckgoSpiceEndpoint
	 */
	protected ApiComDuckduckgoSpiceEndpoint $_api;
	
	/**
	 * Builds a new DuckduckgoWebsite and its api.
	 * @param LoggerInterface $logger
	 * @throws InvalidArgumentException
	 */
	public function __construct(LoggerInterface $logger)
	{
		$this->_logger = $logger;
		$clientFactory = new ClientFactory();
		$clientFactory->setLogger($this->_logger);
		if(!empty(BaseYii::$app->cache))
		{
			$cache = new Psr16ToYii2SimpleCache(BaseYii::$app->cache);
			$cache = new SimpleCacheLogger($cache, $this->_logger);
			$clientFactory->setCache($cache);
		}
		$clientFactory->getConfiguration()->getRetryOptions()->setNbRetries(40);
		$clientFactory->getConfiguration()->disablePreferCurl();
		$client = $clientFactory->createClient();
		$this->_api = new ApiComDuckduckgoSpiceEndpoint($client);
	}
	
	/**
	 * Gets the rates from the given currencies.
	 * 
	 * @param CurrencyCurrency $fromCurrency
	 * @param array<integer, CurrencyCurrency> $toCurrencies
	 * @return ApiComDuckduckgoSpiceResponseInterface
	 * @throws InvalidArgumentException
	 * @throws ParseThrowable
	 * @throws ReificationThrowable
	 * @throws RuntimeException
	 * @throws UnprovidableThrowable
	 */
	public function pullRates(CurrencyCurrency $fromCurrency, array $toCurrencies) : ApiComDuckduckgoSpiceResponseInterface
	{
		$destinationCodes = [];
		
		/** @var CurrencyCurrency $toCurrency */
		foreach($toCurrencies as $toCurrency)
		{
			$destinationCodes[] = $toCurrency->code_iso_4217;
		}
		
		try
		{
			return $this->_api->getRates($fromCurrency->code_iso_4217, $destinationCodes);
		}
		catch(ClientExceptionInterface|UnprovidableJsonException $exc)
		{
			\sleep(5);
			
			try
			{
				return $this->_api->getRates($fromCurrency->code_iso_4217, $destinationCodes);
			}
			catch(ClientExceptionInterface|UnprovidableJsonException $exc)
			{
				\sleep(5);
				
				try
				{
					return $this->_api->getRates($fromCurrency->code_iso_4217, $destinationCodes);
				}
				catch(ClientExceptionInterface|UnprovidableJsonException $exc)
				{
					$message = 'Failed to get rates from {src} to {dst}';
					$context = ['{src}' => $fromCurrency->code_iso_4217, '{dst}' => \implode(',', $destinationCodes)];
					
					throw new RuntimeException(\strtr($message, $context), -1, $exc);
				}
			}
		}
	}
	
}
