<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-currency library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2Currency\Components;

use PhpExtended\Money\CurrencyInterface;
use Yii2Module\Yii2Currency\Models\CurrencyCurrency;

/**
 * CurrencyAdapter class file.
 * 
 * This class is an adapter of the records for the currency interface.
 * 
 * @author Anastaszor
 */
class CurrencyAdapter implements CurrencyInterface
{
	
	/**
	 * The record for the currency.
	 * 
	 * @var CurrencyCurrency
	 */
	protected CurrencyCurrency $_currency;
	
	/**
	 * Builds a new CurrencyAdapter with its active record.
	 * 
	 * @param CurrencyCurrency $currency
	 */
	public function __construct(CurrencyCurrency $currency)
	{
		$this->_currency = $currency;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return $this->getCodeIso4217();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Money\CurrencyInterface::getCodeIso4217()
	 */
	public function getCodeIso4217() : string
	{
		return $this->_currency->code_iso_4217;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Money\CurrencyInterface::getNameEn()
	 */
	public function getNameEn() : string
	{
		return $this->_currency->name_en;
	}
	
}
