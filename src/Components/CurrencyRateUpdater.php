<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-currency library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2Currency\Components;

use InvalidArgumentException;
use PhpExtended\ApiComDuckduckgoSpice\ApiComDuckduckgoSpiceDestinationInterface;
use PhpExtended\ApiComDuckduckgoSpice\ApiComDuckduckgoSpiceResponseInterface;
use RuntimeException;
use Yii2Module\Helper\Components\ObjectUpdater;
use Yii2Module\Yii2Currency\Models\CurrencyRate;

/**
 * CurrencyRateUpdater class file.
 * 
 * This saves all the rates from a given response.
 * 
 * @author Anastaszor
 */
class CurrencyRateUpdater extends ObjectUpdater
{
	
	/**
	 * Saves all the rates from the given response.
	 * 
	 * @param ApiComDuckduckgoSpiceResponseInterface $response
	 * @return integer
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 */
	public function saveRates(ApiComDuckduckgoSpiceResponseInterface $response) : int
	{
		$count = 0;
		
		/** @var ApiComDuckduckgoSpiceDestinationInterface $conversion */
		foreach($response->getTo() as $conversion)
		{
			$rate = $this->saveObjectClass(CurrencyRate::class, [
				'currency_from' => $response->getFrom(),
				'currency_to' => $conversion->getQuotecurrency(),
				'checked' => $response->getTimestamp()->format('Y-m-d'),
			], [
				'value' => (int) (1000000.0 * $conversion->getMid()),
			]);
			
			$count += (int) $rate->isNewRecord;
		}
		
		return $count;
	}
	
}
