<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-currency library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2Currency\Components;

use DateTimeImmutable;
use DateTimeInterface;
use PhpExtended\Money\CurrencyInterface;
use PhpExtended\Money\RateInterface;
use Yii2Module\Yii2Currency\Models\CurrencyRate;

/**
 * RateAdapter class file.
 * 
 * This class is an adapter of the active records to the rate interface.
 * 
 * @author Anastaszor
 */
class RateAdapter implements RateInterface
{
	
	/**
	 * The rate record.
	 * 
	 * @var CurrencyRate
	 */
	protected CurrencyRate $_rate;
	
	/**
	 * Builds a new RateAdapter with its active record.
	 * 
	 * @param CurrencyRate $rate
	 */
	public function __construct(CurrencyRate $rate)
	{
		$this->_rate = $rate;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return '1 '.$this->_rate->currency_from.' = '
			.((string) $this->getValue()).' '.$this->_rate->currency_to;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Money\RateInterface::getSourceCurrency()
	 */
	public function getSourceCurrency() : CurrencyInterface
	{
		return new CurrencyAdapter($this->_rate->currencyFrom);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Money\RateInterface::getTargetCurrency()
	 */
	public function getTargetCurrency() : CurrencyInterface
	{
		return new CurrencyAdapter($this->_rate->currencyTo);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Money\RateInterface::getDatetime()
	 * @psalm-suppress InvalidFalsableReturnType
	 */
	public function getDatetime() : DateTimeInterface
	{
		/** @phpstan-ignore-next-line */ /** @psalm-suppress FalsableReturnStatement */
		return DateTimeImmutable::createFromFormat('Y-m-d', $this->_rate->checked);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Money\RateInterface::getValue()
	 */
	public function getValue() : float
	{
		return ((float) $this->_rate->value) / 100000.0;
	}
	
}
