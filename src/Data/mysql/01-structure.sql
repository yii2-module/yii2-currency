/**
 * Database structure required by CurrencyModule.
 *
 * @author Anastaszor
 * @link https://gitlab.com/yii1-module/yii1-currency
 * @license MIT
 */

SET foreign_key_checks = 0;

DROP TABLE IF EXISTS `currency_currency`;
DROP TABLE IF EXISTS `currency_rate`;
DROP TABLE IF EXISTS `currency_rate_history`;

SET foreign_key_checks = 1;

CREATE TABLE `currency_currency`
(
	`code_iso_4217` CHAR(3) NOT NULL PRIMARY KEY COLLATE ascii_bin COMMENT 'The code iso 4217 of the currency',
	`name_en` VARCHAR(32) NOT NULL COLLATE ascii_bin COMMENT 'The full english name of the currency'
) 
ENGINE=InnoDB DEFAULT CHARSET=ascii COLLATE=ascii_bin;

CREATE TABLE `currency_rate`
(
	`currency_from` CHAR(3) NOT NULL COLLATE ascii_bin COMMENT 'The currency at the beginning of the conversion',
	`currency_to` CHAR(3) NOT NULL COLLATE ascii_bin COMMENT 'The currency at the end of the conversion',
	`checked` DATE NOT NULL COMMENT 'The date when this rate was checked',
	`value` BIGINT(11) NOT NULL COMMENT 'The value, in 1e-6 units of conversion',
	PRIMARY KEY (`currency_from`,`currency_to`,`checked`)
) 
ENGINE=InnoDB DEFAULT CHARSET=ascii COLLATE=ascii_bin;

CREATE TABLE `currency_rate_history`
(
	`currency_from` CHAR(3) NOT NULL COLLATE ascii_bin COMMENT 'The currency at the beginning of the conversion',
	`currency_to` CHAR(3) NOT NULL COLLATE ascii_bin COMMENT 'The currency at the end of the conversion',
	`month` SMALLINT(6) UNSIGNED NOT NULL COMMENT 'The month value (year * 12 + month)',
	`d01` BIGINT(11) DEFAULT NULL COMMENT 'The value on the  1st day of the month',
	`d02` BIGINT(11) DEFAULT NULL COMMENT 'The value on the  2nd day of the month',
	`d03` BIGINT(11) DEFAULT NULL COMMENT 'The value of the  3rd day of the month',
	`d04` BIGINT(11) DEFAULT NULL COMMENT 'The value of the  4th day of the month',
	`d05` BIGINT(11) DEFAULT NULL COMMENT 'The value of the  5th day of the month',
	`d06` BIGINT(11) DEFAULT NULL COMMENT 'The value of the  6th day of the month',
	`d07` BIGINT(11) DEFAULT NULL COMMENT 'The value of the  7th day of the month',
	`d08` BIGINT(11) DEFAULT NULL COMMENT 'The value of the  8th day of the month',
	`d09` BIGINT(11) DEFAULT NULL COMMENT 'The value of the  9th day of the month',
	`d10` BIGINT(11) DEFAULT NULL COMMENT 'The value of the 10th day of the month',
	`d11` BIGINT(11) DEFAULT NULL COMMENT 'The value on the 11st day of the month',
	`d12` BIGINT(11) DEFAULT NULL COMMENT 'The value on the 12nd day of the month',
	`d13` BIGINT(11) DEFAULT NULL COMMENT 'The value of the 13rd day of the month',
	`d14` BIGINT(11) DEFAULT NULL COMMENT 'The value of the 14th day of the month',
	`d15` BIGINT(11) DEFAULT NULL COMMENT 'The value of the 15th day of the month',
	`d16` BIGINT(11) DEFAULT NULL COMMENT 'The value of the 16th day of the month',
	`d17` BIGINT(11) DEFAULT NULL COMMENT 'The value of the 17th day of the month',
	`d18` BIGINT(11) DEFAULT NULL COMMENT 'The value of the 18th day of the month',
	`d19` BIGINT(11) DEFAULT NULL COMMENT 'The value of the 19th day of the month',
	`d20` BIGINT(11) DEFAULT NULL COMMENT 'The value of the 20th day of the month',
	`d21` BIGINT(11) DEFAULT NULL COMMENT 'The value on the 21st day of the month',
	`d22` BIGINT(11) DEFAULT NULL COMMENT 'The value on the 22nd day of the month',
	`d23` BIGINT(11) DEFAULT NULL COMMENT 'The value of the 23rd day of the month',
	`d24` BIGINT(11) DEFAULT NULL COMMENT 'The value of the 24th day of the month',
	`d25` BIGINT(11) DEFAULT NULL COMMENT 'The value of the 25th day of the month',
	`d26` BIGINT(11) DEFAULT NULL COMMENT 'The value of the 26th day of the month',
	`d27` BIGINT(11) DEFAULT NULL COMMENT 'The value of the 27th day of the month',
	`d28` BIGINT(11) DEFAULT NULL COMMENT 'The value of the 28th day of the month',
	`d29` BIGINT(11) DEFAULT NULL COMMENT 'The value of the 29th day of the month',
	`d30` BIGINT(11) DEFAULT NULL COMMENT 'The value of the 30th day of the month',
	`d31` BIGINT(11) DEFAULT NULL COMMENT 'The value on the 31st day of the month',
	PRIMARY KEY (`currency_from`,`currency_to`,`month`),
	INDEX `currency_rate_history_month` (`month`, `currency_from`, `currency_to`)
)
ENGINE=InnoDB DEFAULT CHARSET=ascii COLLATE=ascii_bin;
