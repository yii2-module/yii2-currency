/**
 * Database relations required by CurrencyModule.
 *
 * @author Anastaszor
 * @link https://gitlab.com/yii1-module/yii1-currency
 * @license MIT
 */

ALTER TABLE `currency_rate`
ADD CONSTRAINT `fk_currency_rate_currency_to` 
FOREIGN KEY (`currency_to`) 
REFERENCES `currency_currency` (`code_iso_4217`)
ON UPDATE RESTRICT ON DELETE RESTRICT;

ALTER TABLE `currency_rate`
ADD CONSTRAINT `fk_currency_rate_currency_from` 
FOREIGN KEY (`currency_from`) 
REFERENCES `currency_currency` (`code_iso_4217`)
ON UPDATE RESTRICT ON DELETE RESTRICT;

ALTER TABLE `currency_rate_history`
ADD CONSTRAINT `fk_currency_rate_history_currency_to` 
FOREIGN KEY (`currency_to`) 
REFERENCES `currency_currency` (`code_iso_4217`)
ON UPDATE RESTRICT ON DELETE RESTRICT;

ALTER TABLE `currency_rate_history`
ADD CONSTRAINT `fk_currency_rate_history_currency_from` 
FOREIGN KEY (`currency_from`) 
REFERENCES `currency_currency` (`code_iso_4217`)
ON UPDATE RESTRICT ON DELETE RESTRICT;
